variable "region" {
    description = "The AWS Regions"
    default     = "us-east-1"
}

variable "instance_type" {
    description = "Instance Type"
    default     = "t2.micro"
}