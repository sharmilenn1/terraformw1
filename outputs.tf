output "instance" {
    description = "The IP of instance"
    value       = aws_instance.web.public_ip
}