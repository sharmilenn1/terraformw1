
resource "aws_instance" "web"{
    ami                     = "ami-07caf09b362be10b8"
    instance_type           = "t2.micro"
    subnet_id               = "subnet-0ca3586d43e5d2970"
    vpc_security_group_ids  = ["sg-05711e86eda3d681b"]

    user_data = <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt-get install -y nginx
                echo "Nginx has been installed"
                EOF

    tags = {
        "Terraform" = "true"

    }
}
/*
resource "aws_instance" "webScript" {
    ami                     = "ami-07caf09b362be10b8"
    instance_type           = "t2.micro"

    user_data = <<-EOF
                #!/bin/bash
                sudo apt update
                sudo apt install -y nginx
                echo "Nginx installed"
                EOF

    tags = {
        "Terraform" = "true"
    }
}
*/



